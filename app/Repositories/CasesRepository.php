<?php namespace App\Repositories;

use App\Zaak;

class CasesRepository {

	public function getAll()
	{
		return Zaak::all();
	}

	public function getCase($slug)
	{
		return Zaak::where('slug',$slug)->first();
	}

	public function findOrFail($id)
	{
		return Zaak::findOrFail($id);
	}

}