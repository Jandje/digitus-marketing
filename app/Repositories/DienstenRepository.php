<?php namespace App\Repositories;

use App\Dienst;

class DienstenRepository {

	public function getAll()
	{
		return Dienst::all();
	}

	public function getDienst($slug)
	{
		return Dienst::where('slug', $slug)->first();
	}

	public function findOrFail($id)
	{
		return Dienst::findOrFail($id);
	}

	public function byLabel($val)
	{
		return Dienst::where('label',$val)->get();
	}

}