<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursus extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cursussen';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['afbeelding', 'naam', 'slug', 'content'];

	public function videos()
	{
		return $this->belongsToMany('App\Video');
	}

	public function users()
	{
		return $this->belongsToMany('App\User');
	}

}