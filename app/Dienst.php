<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Dienst extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'diensten';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['icon', 'titel', 'slug', 'label', 'content', 'excerpt'];

}