<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Zaak extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cases';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['logo', 'uitgelichte_afbeelding', 'titel', 'slug', 'klant', 'link_klant', 'project', 'link_project', 'categorie_id', 'user_id', 'content', 'excerpt'];

	public function categorie()
	{
		return $this->belongsTo('App\Categorie');
	}

	public function auteur()
	{
		return $this->belongsTo('App\User');
	}

}