<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Benodigd extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benodigdheden';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sportschoolnaam','logo','foto1','foto2','foto3','foto4','foto5','waaromsportschool','teamfoto','welkomstpagina','bedankpagina','recensie1_foto','recensie2_foto','recensie3_foto','recensie4_foto','recensie5_foto','recensie6_foto','recensie1_tekst','recensie2_tekst','recensie3_tekst','recensie4_tekst','recensie5_tekst','recensie6_tekst','recensie1_naam','recensie2_naam','recensie3_naam','recensie4_naam','recensie5_naam','recensie6_naam',
	];

	public function member()
	{
		return $this->hasMany('App\User');
	}

}