<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['titel', 'content', 'snippet', 'slug', 'user_id', 'categorie_id', 'uitgelichte_afbeelding', 'published_at'];

	public function setPublishedAtAttribute($date)
	{
		$this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
	}

	public function scopePublished($query)
	{
		$query->where('published_at', '<=', Carbon::now());
	}

	public function auteur()
	{
		return $this->belongsTo('App\User');
	}

	public function categorie()
	{
		return $this->belongsTo('App\Categorie');
	}

	public function getAuthor()
	{
		$user = User::findOrFail($this->user_id);
		return $user->voornaam. ' ' .$user->achternaam;
	}

	public function publishedAt()
	{
		$a = date('d F Y', strtotime($this->published_at));
		$henk = explode(' ',$a);
		
		if($henk[1] == 'January')
			{
				$henk[1] = "Januari";
			} elseif ($henk[1] == 'February')
			{
				$henk[1] = "Februari";
			} elseif ($henk[1] == 'March')
			{
				
				$henk[1] = "Maart";
			} elseif ($henk[1] == 'April')
			{
				$henk[1] = "April";
			} elseif ($henk[1] == 'May')
			{
				$henk[1] = "Mei";
			} elseif ($henk[1] == 'June')
			{
				$henk[1] = "Juni";
			} elseif ($henk[1] == 'July')
			{
				$henk[1] = "Juli";
			} elseif ($henk[1] == 'August')
			{
				$henk[1] = "Augustus";
			} elseif ($henk[1] == 'September')
			{
				$henk[1] = "September";
			} elseif ($henk[1] == 'October')
			{
				$henk[1] = "Oktober";
			} elseif ($henk[1] == 'November')
			{
				$henk[1] = "November";
			} elseif ($henk[1] == 'December')
			{
				$henk[1] = "December";
			}

		$gebruikendatum = $henk[0]. ' ' .$henk[1]. ' ' .$henk[2];

		return $gebruikendatum;
	}

}