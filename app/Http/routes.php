<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Homepagina
Route::get('/', 'HomeController@index');

// Trainingen
Route::get('trainingen', 'TrainingenController@index');

// Cases
Route::resource('cases', 'CasesController', ['only'=>['index','show','bycat']]);

// Blog
Route::resource('blog', 'BlogController', ['only'=>['index',  'show']]);

// Diensten
Route::resource('diensten', 'DienstenController', ['only'=>['index',  'show']]);

// Universele bel mij terug route
Route::post('belmijterug', 'ContactController@belMij');

// Contact pagina
Route::get('contact', 'ContactController@index');
Route::post('contact', 'ContactController@getContact');


// Route::get('gratis-demo', 'GratisDemoController@index');
// Route::post('gratis-demo', 'GratisDemoController@gratisDemo');

// Route::get('tabellen', 'TabelController@index');


Route::group(array('prefix'=>'jandje', 'namespace'=>'Admin', 'middleware'=>'is.admin'), function(){

	// Dashboard
	Route::get('', 'AdminController@index');

	// Lijst van alle blogposts
	Route::resource('blog', 'AdminBlogController');

	// Lijst van alle afbeeldingen
	Route::resource('media', 'AdminMediaController');

	// Lijst van diensten
	Route::resource('diensten', 'AdminDienstController');

	// Lijst van alles cases
	Route::resource('cases', 'AdminCasesController');

	// Lijst van alle gebruikers
	Route::resource('users', 'AdminUserController');

	// Maak en update member
	Route::get('member/create', 'AdminUserController@createMember');
	Route::post('member/create', 'AdminUserController@postCreateMember');
	Route::get('member/{id}/edit', 'AdminUserController@getEditMember');
	Route::patch('member/{id}/update', 'AdminUserController@postEditMember');

	// Lijst van alle cursisten
	Route::resource('cursisten', 'AdminCursistenController');

	// Lijst van alle cursussen
	Route::resource('cursus', 'AdminCursusController');

	// Lijst van alle videos voor de cursussen
	Route::resource('videos', 'AdminVideosController');


	// Krijg lijst van benodigdheden
	Route::get('member/{userid}/{benodigdid}', 'AdminBenodigdController@getBenodigd');

	// Lijst van alle recensies
	Route::resource('recensies', 'AdminRecensieController');

	// Lijst van icons die te gebruiken zijn
	Route::get('icons', 'AdminController@icons');

	// Lijst van alle categorieen
	Route::resource('categorieen', 'AdminCategorieController');

	// Lijst van alle rollen
	Route::resource('rollen', 'AdminRollenController');


});

Route::group(array('prefix'=>'jandje', 'namespace'=>'Admin'), function(){

	// Inloggen
	Route::get('login', 'AdminController@getLogin');
	Route::post('login', 'AdminController@postLogin');

	// Uitloggen
	Route::get('logout', 'AdminController@getLogout');

	// Registreren
	Route::get('registreren', 'AdminController@getRegister');
	Route::post('registreren', 'AdminController@postRegister');

});

Route::group(array('namespace'=>'Member'), function(){

	// Inloggen
	Route::get('login', 'MemberAuthController@getLogin');
	Route::post('login', 'MemberAuthController@postLogin');

	// Uitloggen
	Route::get('logout', 'MemberAuthController@getLogout');

	// Password reset
	Route::controller('password', 'PasswordController');

});

Route::group(array('prefix'=>'member', 'namespace'=>'Member', 'middleware'=>'is.member'), function(){

	// Dashboard
	Route::get('', 'MemberIndexController@index');

	// Profiel bewerken
	Route::get('{id}/profiel', 'MemberProfileController@editProfile');
	Route::post('{id}/profile/update', 'MemberProfileController@updateProfile');

	// Wachtwoord wijzigen
	Route::get('{id}/wachtwoord', 'MemberProfileController@editPassword');
	Route::patch('{id}/wachtwoord', 'MemberProfileController@updatePassword');

	// Benodigdheden
	Route::resource('benodigdheden', 'MemberBenodigdhedenController');

	// Cursussen
	Route::resource('cursussen', 'MemberCursussenController');

	// Videos
	Route::resource('{cursusslug}/videos', 'MemberVideoController', ['only'=>['index', 'show']]);
	Route::get('video1', function(){
		return view('member.videos.video1');
	});

});



// Route::get('home', 'HomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);