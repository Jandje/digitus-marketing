<?php namespace App\Http\Controllers;

use App\Repositories\DienstenRepository;

class DienstenController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $dienstrepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(DienstenRepository $dienstrepository)
	{
		$this->dienstrepository = $dienstrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$diensten = $this->dienstrepository->getAll();
		return view('diensten.index')
			->with(['diensten'=>$diensten]);
	}

	public function show($slug)
	{
		$dienst = $this->dienstrepository->getDienst($slug);
		return view('diensten.dienst')
			->with(['dienst'=>$dienst]);

	}

}
