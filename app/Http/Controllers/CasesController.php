<?php namespace App\Http\Controllers;

use App\Repositories\DienstenRepository;
use App\Repositories\CasesRepository;

class CasesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $dienstrepository;
	private $casesrepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(DienstenRepository $dienstrepository, CasesRepository $casesrepository)
	{
		$this->dienstrepository = $dienstrepository;
		$this->casesrepository = $casesrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cases = $this->casesrepository->getAll();
		return view('cases.index')
			->with(['cases'=>$cases]);
	}

	public function show($slug)
	{
		$case = $this->casesrepository->getCase($slug);
		return view('cases.case')
			->with(['case'=>$case]);

	}

	public function bycat($val)
	{

	}

}
