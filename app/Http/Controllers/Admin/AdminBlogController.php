<?php namespace App\Http\Controllers\Admin;

use App\Categorie;
use App\Post;
use App\Repositories\PostRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogPostRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;

use Intervention\Image\ImageManagerStatic as Image;

class AdminBlogController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $postrepository;

	protected $dates = ['published_at'];

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(PostRepository $postrepository)
	{
		$this->postrepository = $postrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = $this->postrepository->getAllLatestPosts();

		return view('admin.blog.index', compact('posts'));
	}

	public function create()
	{
		$categorieen = Categorie::all();
		return view('admin.blog.create', compact('categorieen'));
	}

	public function store(Post $post, BlogPostRequest $request)
	{

		if($request->file('uitgelichte_afbeelding'))
		{
			$file 				= $request->file('uitgelichte_afbeelding');
			$destinationPath 	= 'images/blogposts';
			$filename 			= $file->getClientOriginalName();
			$encoded 			= urldecode($filename);
			$image 				= Image::make($file)->widen(728)->crop(728, 380)->save('images/blogposts/'.$encoded);
			// $upload_success 	= $file->move($destinationPath, $encoded);
			$imagepath			= 'images/blogposts/'.$encoded;
		} else {
			$imagepath = null;
		}

		// dd($image);

		$newpost = new $post(array(
			'titel'						=> $request->get('titel'),
			'content'					=> $request->get('content'),
			'snippet'					=> strip_tags($request->get('content')),
			'slug'						=> str_slug($request->get('titel'), '-'),
			'user_id'					=> $request->get('user_id'),
			'categorie_id'				=> $request->get('categorie_id'),
			'uitgelichte_afbeelding'	=> $imagepath,
			'published_at'				=> $request->get('published_at')
		));

		$newpost->save();

		return redirect('jandje/blog');
	}

	public function edit($id)
	{
		$post = $this->postrepository->findOrFail($id);
		$categorieen = Categorie::all();
		return view('admin.blog.edit', compact(['categorieen', 'post']));
	}

	public function update($id, BlogPostRequest $request)
	{
		$post = $this->postrepository->findOrFail($id);

		if($request->hasFile('uitgelichte_afbeelding'))
		{
			$file 				= $request->file('uitgelichte_afbeelding');
			$destinationPath 	= 'images/blogposts';
			$filename 			= $file->getClientOriginalName();
			$encoded 			= urldecode($filename);
			$upload_success 	= $file->move($destinationPath, $encoded);
			$image 				= str_replace('\\', '/', $upload_success);
		} else {
			$image = $post->uitgelichte_afbeelding;
		}

		$post->update(array(
			'titel'						=> $request->get('titel'),
			'content'					=> $request->get('content'),
			'snippet'					=> strip_tags($request->get('content')),
			'slug'						=> str_slug($request->get('titel'), '-'),
			'user_id'					=> $request->get('user_id'),
			'categorie_id'				=> $request->get('categorie_id'),
			'uitgelichte_afbeelding'	=> $image,
		));

		return redirect('jandje/blog');
	}

	public function destroy(Post $post, $id)
	{
		$post = $post->find($id);

		$post->delete();

		return redirect()->back();
	}

}
