<?php namespace App\Http\Controllers\Admin;

use App\User;
use App\Benodigd;
use App\Http\Controllers\Controller;

class AdminBenodigdController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getBenodigd($user_id, $benodigd_id, User $user, Benodigd $benodigd)
	{
		$member = $user->findOrFail($user_id);
		$benodigdheid = $benodigd->findOrFail($benodigd_id);

		return view('admin.users.benodigdheden.index', compact('benodigdheid'));
	}

}
