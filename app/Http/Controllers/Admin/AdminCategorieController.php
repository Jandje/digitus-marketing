<?php namespace App\Http\Controllers\Admin;

use App\Categorie;
use App\Repositories\CategorieRepository;
use App\Http\Requests\CategorieRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminCategorieController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $categorierepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(CategorieRepository $categorierepository)
	{
		$this->categorierepository = $categorierepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categorieen = $this->categorierepository->getAll();

		return view('admin.categorie.index', compact(['categorieen']));
	}

	public function create()
	{
		return view('admin.categorie.create');
	}

	public function store(Categorie $categorie, CategorieRequest $request)
	{
		$newcat = new $categorie(array(
			'naam'	=> $request->get('naam'),
		));

		$newcat->save();

		return redirect('jandje/categorieen');
	}

	public function edit($id)
	{
		$categorie = $this->categorierepository->findOrFail($id);
		return view('admin.categorie.edit', compact(['categorie']));
	}

	public function update($id, CategorieRequest $request)
	{
		$categorie = $this->categorierepository->findOrFail($id);

		$categorie->update([
			'naam'	=> $request->get('naam'),
		]);

		return redirect('jandje/categorieen');
	}

	public function destroy($id)
	{
		$categorie = $this->categorierepository->findOrFail($id);

		$categorie->delete();

		return redirect()->back();
	}

}
