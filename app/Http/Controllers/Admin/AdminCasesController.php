<?php namespace App\Http\Controllers\Admin;

use App\Categorie;
use App\Zaak;
use App\Repositories\CasesRepository;
use App\Http\Requests\CaseRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminCasesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $casesrepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(CasesRepository $casesrepository)
	{
		$this->casesrepository = $casesrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cases = $this->casesrepository->getAll();

		return view('admin.cases.index', compact(['cases']));
	}

	public function create()
	{
		$categorieen = Categorie::all();
		return view('admin.cases.create', compact('categorieen'));
	}

	public function store(Zaak $case, CaseRequest $request)
	{

		$file1 				= $request->file('uitgelichte_afbeelding');
		$destinationPath 	= 'images/cases';
		$file1name 			= $file1->getClientOriginalName();
		$encoded1 			= urldecode($file1name);
		$upload_success1 	= $file1->move($destinationPath, $encoded1);
		$image1				= str_replace('\\', '/', $upload_success1);

		$file2 				= $request->file('logo');
		$file2name 			= $file2->getClientOriginalName();
		$encoded2 			= urldecode($file2name);
		$upload_success2 	= $file2->move($destinationPath, $encoded2);
		$image2				= str_replace('\\', '/', $upload_success2);

		if($request->has('excerpt'))
		{
			$excerpt = $request->get('excerpt');
		} else {
			$excerpt = substr($request->get('content'),0,250).'...';
		}

		$newcase = new $case(array(
			'titel'						=> $request->get('titel'),
			'content'					=> $request->get('content'),
			'excerpt'					=> $excerpt,
			'slug'						=> str_slug($request->get('titel'), '-'),
			'user_id'					=> $request->get('user_id'),
			'categorie_id'				=> $request->get('categorie_id'),
			'uitgelichte_afbeelding'	=> $image1,
			'logo'						=> $image2,
			'klant'						=> $request->get('klant'),
			'link_klant'				=> $request->get('link_klant'),
			'link_project'				=> $request->get('link_project'),
		));

		$newcase->save();

		return redirect('jandje/cases');
	}

	public function edit($id)
	{
		$case = $this->casesrepository->findOrFail($id);
		$categorieen = Categorie::all();
		return view('admin.cases.edit', compact(['case', 'categorieen']));
	}

	public function update($id, CaseRequest $request)
	{
		$case = $this->casesrepository->findOrFail($id);

		if($request->has('excerpt'))
		{
			$excerpt = $request->get('excerpt');
		} else {
			$excerpt = substr($request->get('content'),0,250).'...';
		}

		if($request->hasFile('uitgelichte_afbeelding'))
		{
			$file1 				= $request->file('uitgelichte_afbeelding');
			$destinationPath 	= 'images/cases';
			$file1name 			= $file1->getClientOriginalName();
			$encoded1 			= urldecode($file1name);
			$upload_success1 	= $file1->move($destinationPath, $encoded1);
			$image1				= str_replace('\\', '/', $upload_success1);
		} else {
			$image1 = $case->uitgelichte_afbeelding;
		}

		if($request->hasFile('logo'))
		{
			$file2 				= $request->file('logo');
			$file2name 			= $file2->getClientOriginalName();
			$encoded2 			= urldecode($file2name);
			$upload_success2 	= $file2->move($destinationPath, $encoded2);
			$image2				= str_replace('\\', '/', $upload_success2);
		} else {
			$image2 = $case->logo;
		}

		$case->update([
			'titel'						=> $request->get('titel'),
			'content'					=> $request->get('content'),
			'excerpt'					=> $excerpt,
			'slug'						=> str_slug($request->get('titel'), '-'),
			'user_id'					=> $request->get('user_id'),
			'categorie_id'				=> $request->get('categorie_id'),
			'uitgelichte_afbeelding'	=> $image1,
			'logo'						=> $image2,
			'klant'						=> $request->get('klant'),
			'link_klant'				=> $request->get('link_klant'),
			'link_project'				=> $request->get('link_project'),
		]);

		return redirect('jandje/cases');
	}

	public function destroy($id)
	{
		$case = $this->casesrepository->findOrFail($id);

		$case->delete();

		return redirect()->back();
	}

}
