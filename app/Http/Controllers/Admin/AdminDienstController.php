<?php namespace App\Http\Controllers\Admin;

use App\Dienst;
use App\Repositories\DienstenRepository;
use App\Http\Requests\DienstRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminDienstController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $dienstenrepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(DienstenRepository $dienstenrepository)
	{
		$this->dienstenrepository = $dienstenrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$diensten = $this->dienstenrepository->getAll();

		return view('admin.diensten.index', compact(['diensten']));
	}

	public function create()
	{
		return view('admin.diensten.create');
	}

	public function store(Dienst $dienst, DienstRequest $request)
	{

		$newdienst = new $dienst([
			'icon'			=> $request->get('icon'),
			'titel'			=> $request->get('titel'),
			'slug'			=> str_slug($request->get('titel'), '-'),
			'label'			=> $request->get('label'),
			'content'		=> $request->get('content'),
		]);

		$newdienst->save();

		// Stuur email naar emailadres met inloggegevens

		return redirect('jandje/diensten');
	}

	public function edit($id)
	{
		$dienst = $this->dienstenrepository->findOrFail($id);
		return view('admin.diensten.edit', compact(['dienst']));
	}

	public function update($id, DienstRequest $request)
	{
		$dienst = $this->dienstenrepository->findOrFail($id);

		$dienst->update([
			'icon'		=> $request->get('icon'),
			'titel'		=> $request->get('titel'),
			'slug'		=> str_slug($request->get('titel'), '-'),
			'label'		=> $request->get('label'),
			'content'	=> $request->get('content'),
		]);

		return redirect('jandje/diensten');
	}

	// public function destroy($id)
	// {
	// 	$user = $this->userrepository->findOrFail($id);

	// 	$user->delete();

	// 	return redirect()->back();
	// }

}
