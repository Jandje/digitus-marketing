<?php namespace App\Http\Controllers\Admin;

use App\Recensie;
use App\Repositories\RecensieRepository;
use App\Http\Requests\RecensieRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminRecensieController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $recensiesrepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(RecensieRepository $recensierepository)
	{
		$this->recensierepository = $recensierepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$recensies = $this->recensierepository->getAllRecensies();

		return view('admin.recensies.index', compact(['recensies']));
	}

	public function create()
	{
		return view('admin.recensies.create');
	}

	public function store(RecensieRequest $request, Recensie $recensie)
	{

		$newrecensie = new $recensie([
			'klantnaam'	=> $request->get('klantnaam'),
			'content'	=> $request->get('content'),
			'bedrijf'	=> $request->get('bedrijf'),
			'link'		=> $request->get('link'),
		]);

		$newrecensie->save();

		return redirect('jandje/recensies');
	}

	public function edit($id)
	{
		$recensie = $this->recensierepository->findOrFail($id);
		return view('admin.recensies.edit', compact(['recensie']));
	}

	public function update($id, RecensieRequest $request)
	{
		$recensie = $this->recensierepository->findOrFail($id);

		$recensie->update([
			'klantnaam'	=> $request->get('klantnaam'),
			'content'	=> $request->get('content'),
			'bedrijf'	=> $request->get('bedrijf'),
			'link'		=> $request->get('link'),
		]);

		return redirect('jandje/recensies');
	}

	public function destroy($id)
	{
		$recensie = $this->recensierepository->findOrFail($id);

		$recensie->delete();

		return redirect()->back();
	}

}
