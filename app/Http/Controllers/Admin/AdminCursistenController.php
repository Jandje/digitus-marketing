<?php namespace App\Http\Controllers\Admin;

use App\Cursist;
use App\Repositories\CursusRepository;
use App\Http\Requests\CursusRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminCursistenController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $cursusrepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(CursusRepository $cursusrepository)
	{
		$this->cursusrepository = $cursusrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Pak alle cursussen
		$cursussen = $this->cursusrepository->getAllCursussen();

		// In view alle cursussen laten zien met bijbehorende cursisten
		return view('admin.cursist.index', compact(['cursussen']));
	}

}
