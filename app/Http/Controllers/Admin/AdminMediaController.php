<?php namespace App\Http\Controllers\Admin;

use App\Image;
use App\Repositories\ImagesRepository;
use App\Http\Requests\MediaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminMediaController extends Controller {

	public function __construct(ImagesRepository $imagesrepository)
	{
		$this->imagesrepository = $imagesrepository;
	}

	public function index()
	{
		$images = $this->imagesrepository->getPaginateAllImages(15);
		return view('admin.media.index', compact('images'));
	}

	public function create()
	{
		return view('admin.media.create');
	}

	public function store(MediaRequest $request, Image $media)
	{
		$file 				= $request->file('url');
		$destinationPath 	= 'images/media';
		$filename 			= $file->getClientOriginalName();
		$encoded 			= urldecode($filename);
		$upload_success 	= $file->move($destinationPath, $encoded);
		$image 				= str_replace('\\', '/', $upload_success);

		$newimage = new $media(array(
			'titel'			=> $request->get('titel'),
			'alt'			=> $request->get('alt'),
			'url'			=> $image,
		));

		$newimage->save();

		return redirect('jandje/media');
		
	}

	public function edit($id)
	{
		$image = $this->imagesrepository->findOrFail($id);
		return view('admin.media.edit', compact('image'));
	}

	public function update(MediaRequest $request, $id)
	{
		$media = $this->imagesrepository->findOrFail($id);

		if($request->hasFile('url'))
		{
			$file 				= $request->file('url');
			$destinationPath 	= 'images/media';
			$filename 			= $file->getClientOriginalName();
			$encoded 			= urldecode($filename);
			$upload_success 	= $file->move($destinationPath, $encoded);
			$image 				= str_replace('\\', '/', $upload_success);
		} else {
			$image = $media->url;
		}

		$media->update([
			'titel'			=> $request->get('titel'),
			'alt'			=> $request->get('alt'),
			'afbeelding'	=> $image,
		]);

		return redirect('jandje/media');
		
	}

	public function destroy($id)
	{
		$image = $this->imagesrepository->findOrFail($id);
		
		$image->delete();

		return redirect()->back();
		
	}

}