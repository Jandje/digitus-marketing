<?php namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use App\Repositories\PostRepository;
use App\Http\Requests\GratisDemoRequest;
use Mail;

class GratisDemoController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('gratis-demo.index');
	}

	public function gratisDemo(GratisDemoRequest $request, User $user)
	{
		// Stel verloopdatum voor de gratis demo in
		$verloopdatum = Carbon::now('Europe/Amsterdam')->addWeeks(2);

		$newuser = new $user([
			'voornaam'		=> $request->get('voornaam'),
			'achternaam'	=> $request->get('achternaam'),
			'email'			=> $request->get('email'),
			'password'		=> bcrypt($request->get('wachtwoord')),
			'plan'			=> 'gratis-demo',
			'role_id'		=> '2',
			'benodigdheden' => '1',
			'verloopdatum'	=> $verloopdatum,
		]);

		$data = array(
			'voornaam'		=> $request->get('voornaam'),
			'achternaam'	=> $request->get('achternaam'),
			'email'			=> $request->get('email'),
			'verloopdatum'	=> $verloopdatum,
		);

		// Stuur email naar emailadres met inloggegevens
		// Mail::send('includes.email.gratis-demo', 
		// 	array('voornaam'=>$request->get('voornaam'), 'achternaam'=>$request->get('achternaam'), 'email'=>$request->get('email')), function($message) use ($data)
		// 	{
		// 		// $message->from('info@digitusmarketing.nl', 'Digitus Marketing');
		// 		$message->to($data['email'], $data['voornaam']. ' '.$data['achternaam'])->subject('Gratis Demo Digitus Marketing');
		// 	});

		$newuser->save();


		return redirect('login');
	}

}
