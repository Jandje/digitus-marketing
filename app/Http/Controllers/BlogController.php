<?php namespace App\Http\Controllers;

use App\Repositories\DienstRepository;
use App\Repositories\PostRepository;
use App\User;

class BlogController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $postrepository;

	protected $dates = ['published_at'];

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(PostRepository $postrepository)
	{
		$this->postrepository = $postrepository;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = $this->postrepository->paginateFour();
		return view('blog.index')
			->with(['blogposts'=>$posts]);
	}

	public function show($slug)
	{
		$post = $this->postrepository->getPost($slug);
		$categorie = $post->categorie_id;

		$relatedposts = $this->postrepository->byCat($categorie);

		return view('blog.post', compact('post','relatedposts'));

	}

}
