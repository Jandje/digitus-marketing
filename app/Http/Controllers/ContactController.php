<?php namespace App\Http\Controllers;

use Mail;
use App\Http\Requests\BelMijTerugRequest;
use App\Http\Requests\ContactControllerRequest;

class ContactController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('contact.index');
	}

	public function getContact(ContactControllerRequest $request)
	{

		// Verstuur contactformulier
		Mail::send('includes.email.contactformulier', 
			array('naam'=>$request->get('naam'), 'email'=>$request->get('email'), 'bericht'=>$request->get('bericht')), function($message) use($request)
			{
				// $message->from($request['email'], $request['naam']);
				$message->to('info@digitusmarketing.nl', 'Digitus Marketing')->subject('Contactformulier Digitus Marketing');
			});

		return redirect()->back()->with('contactmessage', 'We hebben je bericht ontvangen en nemen zo spoedig mogelijk contact met je op!');
	}

	public function belMij(BelMijTerugRequest $request)
	{
		// Verstuur belmijterug verzoek
		Mail::send('includes.email.belmijterug', 
			array('naam'=>$request->get('naam'), 'telefoonnummer'=>$request->get('telefoonnummer')), function($message) use($request)
			{
				// $message->from($request['email'], $request['naam']);
				$message->to('info@digitusmarketing.nl', 'Digitus Marketing')->subject('Bel mij terug verzoek');
			});

		return redirect()->back()->with('belmijterugmessage', 'We hebben je bericht ontvangen en nemen zo spoedig mogelijk contact met je op!');
	}

}
