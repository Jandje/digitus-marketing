<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Recensie extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'recensies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['klantnaam', 'content', 'bedrijf', 'link'];

}