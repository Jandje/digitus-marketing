<?php namespace App\Providers;

use App\Dienst;
use App\Recensie;
use App\Post;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer(['includes.footer.mainfooter','includes.snellepage'], function($view)
		{
			$view->with('diensten', Dienst::all());
		});
		view()->composer('includes.recensies.klanten', function($view)
		{
			$view->with('recensies', Recensie::all());
		});

		view()->composer('includes.sidebar.groot', function($view)
		{
			$view->with('posts', Post::latest('created_at')->get()->take(5));
		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
