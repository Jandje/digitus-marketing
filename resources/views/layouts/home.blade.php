<!DOCTYPE html>

<html lang="nl-NL">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Digitus Marketing</title>
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<!--[if lt IE 9]>
		<script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
	<![endif]-->
@yield('css')
@yield('meta')
@yield('pixels')

</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.3&appId=387796737964493";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

@include('includes.menus.sidemenu')

<div id="page">
@yield('menu')
<div class="spacial"></div>

@yield('uitgelicht')

<div id="s-p-i">
	<div class="container">
		@yield('snellepage')
	</div>
</div>

<div id="content">
	<div class="container">
		<div class="col-md-8 col-lg-8">
			@yield('content')
		</div>
		<div class="col-md-4 col-lg-4">
			@yield('sidebar')
		</div>
	</div>
</div>
@yield('footer')
</div>

@yield('footerscript')
{!! Html::script('js/home.js') !!}

</body>
</html>