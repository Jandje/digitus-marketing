@extends('layouts.detail')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
<div class="blog blog-post box">
	<div class="-inblog-post-image">
		<img src="{!! url($case->uitgelichte_afbeelding) !!}" alt="">
	</div>
	<div class="blog-post-content">
		<div class="pull-right dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" id="dropdownMenu1">
				meer <span class="caret"></span>
			</a>
			<ul class="dropdown-menu meeropties box" role="menu" aria-labelledby="dropdownMenu1">
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-facebook"></span> Deel op Facebook</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-twitter"></span> Deel op Twitter</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-pencil"></span> Reageer</a></li>
			</ul>
		</div>
		<div class="blog-titel in-post">
			<h3>{!! $case->titel !!}</h3>
		</div>
		<div class="case-logo">
					<a href="{!! url($case->link_klant) !!}" class="case-klant" rel="nofollow"><img src="{!! url($case->logo) !!}" class="case-thumbnail" alt=""></a>
				</div>
				<div class="case-bottom">
					<a href="{!! url($case->link_klant) !!}" class="case-klant" rel="nofollow">{{ $case->klant }}</a>
					<span class="glyphicon glyphicon-link"></span> {!! $case->categorie->naam !!}
				</div>
		<div class="clearfix"></div>
		<hr>
		<div class="blog-content">
			{!! $case->content !!}
		</div>
	</div>
</div>

@stop
@section('sidebar')
	@include('includes.sidebar.groot')
@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	{!! Html::script('js/bjqs.js') !!}
	<script>
		var menuheight = $('div#menubars').height();

		$(document).ready(function(){
		  $('div.spacial').css('height',menuheight);
		});
	  $('.dropdown-toggle').dropdown();

	  $(document).ready(function(){
		$('#recensies-slider').bjqs();

		$('ol.bjqs-markers.h-centered').css('left','0 !important');
		$("#banner-fade").css({"display":"block","margin":"0 auto"});
		$("#slider-banner").css({"display":"block","margin":"0 auto"});
		$('#banner-fade li.bjqs-slide').css('width',$('#banner-fade .bjqs-wrapper').width());
		});
	</script>
@stop