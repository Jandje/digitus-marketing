@extends('layouts.overview')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
		<div class="box introductie">
			<div class="titel">
				<h3><span class="glyphicon glyphicon-briefcase"></span> Cases</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente unde ullam praesentium ab tenetur, sit error tempore consectetur odio. Autem culpa similique ea voluptate magnam. Possimus, molestiae enim. Voluptates, officiis!</p>
			</div>
		</div>
@stop

@section('diensten')
	@foreach($cases as $case)
	<div class="col-md-6 col-lg-6">
		<div class="case box">
			<div class="case-image">
				<img src="{!! url($case->uitgelichte_afbeelding) !!}" alt="">
			</div>
			<div class="case-content">
				<div class="pull-right dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" id="dropdownMenu1">
						meer <span class="caret"></span>
					</a>
					<ul class="dropdown-menu meeropties box" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-facebook"></span> Deel op Facebook</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-twitter"></span> Deel op Twitter</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-pencil"></span> Reageer</a></li>
					</ul>
				</div>
				<p class="case-publish">{{$case->created_at}}</p>
				<div class="case-titel">
					<h3>{!! $case->titel !!}</h3>
				</div>
				<div class="case-lezen">
					<a href="{{ action('CasesController@show',[$case->slug]) }}" class="btn nrml-button">Lees meer</a>
				</div>
				<div class="case-logo">
					<a href="{!! url($case->link_klant) !!}" class="case-klant" rel="nofollow"><img src="{!! url($case->logo) !!}" class="case-thumbnail" alt=""></a>
				</div>
				<div class="case-bottom">
					<a href="{!! url($case->link_klant) !!}" class="case-klant" rel="nofollow">{{ $case->klant }}</a>
					<span class="glyphicon glyphicon-link"></span> {!! $case->categorie->naam !!}
				</div>
			</div>
		</div>
	</div>
	@endforeach
@stop

@section('sidebar')
	@include('includes.sidebar.belmijterug')
@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	@include('includes.forms.belmijterugformulier')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
@stop