@extends('layouts.overview')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
		<div class="box introductie">
			<div class="titel">
				<h3>Leren doe je door te doen!</h3>
			</div>
			<div class="content">
				<p>Tijdens onze workshops breng je direct in praktijk wat je hebt geleerd.<br />Want zonder in praktijk te brengen wat je hebt geleerd, weten we allebei niet<br/>of je het begrijpt en zelfstandig kunt toepassen.</p>
				<b class="bold">Wij gaan voor resultaat!</b>
			</div>
		</div>

@stop
@section('diensten')
	@foreach($diensten as $dienst)
		<div class="col-md-3 s-p">
			<div class="s-p-i box">
				<span class="{!! $dienst->icon !!}"></span>
				<h2 class="titel">{!! $dienst->titel !!}</h2>
				<a class="btn nrml-button" href="{!! action('DienstenController@show', [$dienst->slug] ) !!}">Lees meer</a>
			</div>
		</div>
	@endforeach
@stop
@section('sidebar')
	@include('includes.sidebar.belmijterug')
@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	@include('includes.forms.belmijterugformulier')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
@stop