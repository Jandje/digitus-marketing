@extends('layouts.detail')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
<div class="blog blog-post box">
	<div class="blog-post-content">
		<div class="pull-right dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" id="dropdownMenu1">
				meer <span class="caret"></span>
			</a>
			<ul class="dropdown-menu meeropties box" role="menu" aria-labelledby="dropdownMenu1">
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-facebook"></span> Deel op Facebook</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-twitter"></span> Deel op Twitter</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa fa-pencil"></span> Reageer</a></li>
			</ul>
		</div>
		<div class="blog-titel in-post">
			<h3><span class="{{$dienst->icon}}"></span>	{!! $dienst->titel !!}</h3>
		</div>
		<div class="clearfix"></div>
		<hr>
		<div class="blog-content">
			{!! $dienst->content !!}
		</div>
	</div>
</div>
@stop
@section('sidebar')
	@include('includes.sidebar.belmijterug')
	@include('includes.sidebar.groot')
@stop
@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	@include('includes.forms.belmijterugformulier')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
@stop