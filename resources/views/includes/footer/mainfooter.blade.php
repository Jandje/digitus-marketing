<div id="footer">
	<div class="container">
		<div class="col-md-3 footer-adres">
			<h3>Digitus Marketing</h3>
			<address>
				<p>Industriestraat 215a<br/>
				7553CP Hengelo(ov)</p>
				<p>Telefoon: 085 003 02 56<br/>
				info@digitusmarketing.nl</p>
				<p>Kvk: 52622746<br />
				BTW: NL.8505.25.469.B.01<br/>
				ABN Amro: 59.59.22.694</p>
			</address>
			{{-- <button class="btn nrml-button">Plan uw route</button> --}}
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-3 footer-diensten">
			<h3>Diensten</h3>
			<ul class="footer list-buttons">
			@foreach($diensten as $dienst)
				<li>
					<a href="{!! action('DienstenController@show', [$dienst->slug]) !!}">
						<span class="{!! $dienst->icon !!}"></span> {!! $dienst->titel !!}
					</a>
				</li>
			@endforeach
			</ul>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4 footer-contact-formulier">
			@include('includes.forms.contactformulier')
		</div>
	</div>
		
</div>
<div id="footer-map">
</div>
{{-- <div id="footer-aanmelden">
	<div class="container">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="col-md-6">
				<a class="btn demo-button" href="">Aanmelden</a>
			</div>
			<div class="col-md-6">
				<a class="btn demo-button">Gratis demo</a>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>
</div> --}}
<div id="footer-rechten">
	<div class="container">
		<p class="website-door">
			<a href="#" class="website-door">Maarten Vazquez Creatiestudio 2015</a> / <a href="#">Voorwaarden</a>
		</p>
	</div>
</div>