<div id="sidemenu">
	<div id="close-sidemenu">
		<button type="button" class="close pull-left" style="font-size:22pt !important;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span> </button>
	</div>
	<ul class="list-buttons">
		@if(Request::is('diensten') || Request::is('diensten/*'))
			<li class="active"><a href="{!! action('DienstenController@index') !!}">Diensten</a><div class="menu-marker"></div></li>
		@else
			<li><a href="{!! action('DienstenController@index') !!}">Diensten</a><div class="menu-marker"></div></li>
		@endif
		@if(Request::is('cases') || Request::is('cases/*'))
			<li class="active"><a href="{!! action('CasesController@index') !!}">Cases</a><div class="menu-marker"></div></li>
		@else
			<li><a href="{!! action('CasesController@index') !!}">Cases</a><div class="menu-marker"></div></li>
		@endif
		@if(Request::is('trainingen') || Request::is('trainingen/*'))
			<li class="active"><a href="{!! action('TrainingenController@index') !!}">Trainingen</a><div class="menu-marker"></div></li>
		@else
			<li><a href="{!! action('TrainingenController@index') !!}">Trainingen</a><div class="menu-marker"></div></li>
		@endif
		@if(Request::is('blog') || Request::is('blog/*'))
			<li class="active"><a href="{{ action('BlogController@index') }}">Blog</a><div class="menu-marker"></div></li>
		@else
			<li><a href="{{ action('BlogController@index') }}">Blog</a><div class="menu-marker"></div></li>
		@endif
		@if(Request::is('contact'))
			<li class="active"><a href="{{ action('ContactController@index') }}">Contact</a><div class="menu-marker"></div></li>
		@else
			<li><a href="{{ action('ContactController@index') }}">Contact</a><div class="menu-marker"></div></li>
		@endif
	</ul>
	<div class="social-sidemenu">
		<ul>
			<li>F</li>
			<li>T</li>
			<li>in</li>
			<li>G+</li>
			<li>Mail</li>
		</ul>
	</div>
</div>