<div id="mainmenubar">
	<div class="container">
		<div class="col-md-2 logo">
			<a href="{{ action('HomeController@index') }}" title="Digitus Marketing">
				<img src="{{ URL::to('/') }}/images/digitus/Digitus-logo-wit.svg" onerror="this.src='{{ URL::to('/') }}/images/digitus/Digitus-logo-wit.png'" alt="" class="logo">
			</a>
		</div>
		<div class="menubar col-md-10">
			<nav class="col-md-9 desktop">
				<ul>
					@if(Request::is('diensten') || Request::is('diensten/*'))
						<li class="active"><a href="{!! action('DienstenController@index') !!}">Diensten</a><div class="menu-marker"></div></li>
					@else
						<li><a href="{!! action('DienstenController@index') !!}">Diensten</a><div class="menu-marker"></div></li>
					@endif
					@if(Request::is('cases') || Request::is('cases/*'))
						<li class="active"><a href="{!! action('CasesController@index') !!}">Cases</a><div class="menu-marker"></div></li>
					@else
						<li><a href="{!! action('CasesController@index') !!}">Cases</a><div class="menu-marker"></div></li>
					@endif
					@if(Request::is('trainingen') || Request::is('trainingen/*'))
						<li class="active"><a href="{!! action('TrainingenController@index') !!}">Trainingen</a><div class="menu-marker"></div></li>
					@else
						<li><a href="{!! action('TrainingenController@index') !!}">Trainingen</a><div class="menu-marker"></div></li>
					@endif
					@if(Request::is('blog') || Request::is('blog/*'))
						<li class="active"><a href="{{ action('BlogController@index') }}">Blog</a><div class="menu-marker"></div></li>
					@else
						<li><a href="{{ action('BlogController@index') }}">Blog</a><div class="menu-marker"></div></li>
					@endif
					@if(Request::is('contact'))
						<li class="active"><a href="{{ action('ContactController@index') }}">Contact</a><div class="menu-marker"></div></li>
					@else
						<li><a href="{{ action('ContactController@index') }}">Contact</a><div class="menu-marker"></div></li>
					@endif
				</ul>
			</nav>
			<div class="rechter-menu col-md-3">
				<div class="demo-holder pull-left">
					{{-- <a class="btn demo-button" id="demo-button" href="">Gratis Demo</a> --}}
				</div>
				<div class="sidemenu pull-right mobiel">
					<a class="sidemenu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span></a>
				</div>
			</div>
		</div>
	</div>
</div>