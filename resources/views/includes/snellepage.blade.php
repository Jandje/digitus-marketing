@foreach($diensten as $dienst)
	<div class="col-md-3 s-p">
		<div class="s-p-i box">
			<span class="{!! $dienst->icon !!}"></span>
			<h2 class="titel">{!! $dienst->titel !!}</h2>
			<a class="btn nrml-button" href="{!! action('DienstenController@show', [$dienst->slug]) !!}">Lees meer</a>
		</div>
	</div>
@endforeach