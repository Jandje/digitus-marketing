<div id="sidebar-groot" class="desktop">
	<div class="box">
		<div class="fb-page" data-href="https://www.facebook.com/digitusmarketing" data-width="348" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/digitusmarketing"><a href="https://www.facebook.com/digitusmarketing">Digitus Marketing</a></blockquote></div></div>
		<hr>
		<div class="blogroll">
			<h4>Recente blogs</h4>
			<ul class="list-buttons">
			@foreach($posts as $post)
				<li>
					<a href="{!! url('blog/'.$post->slug) !!}">
					<p>{!! $post->titel !!}</p>
						<span class="glyphicon glyphicon-triangle-right pull-right"></span>
					</a>
				</li>
			@endforeach
			</ul>
			<a class="btn nrml-button" href="{{ url('blog') }}">Lees meer</a>
		</div>
		<hr>
		<div class="download-ebook">
			<h4>Download ons e-book</h4>
			<img src="http://www.digitusmarketing.nl/uploads/images/ebooks/ebook-plank.png" alt="" class="ebook-uitgelicht">
			<button class="btn demo-button" id="download-ebook-sidebar">Download</button>
		</div>
	</div>
</div>