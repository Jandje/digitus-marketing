<div id="sidebar-klein">
	<div class="box bel-mij-terug">
		@if(Session::has('belmijterugmessage'))
			<div class="alert alert-info">
				{{ session('belmijterugmessage') }}
			</div>
		@else
		<h3>Welke workshop is geschikt voor jou?</h3>
		<p>Klik op onderstaande button als je wilt weten welke workshop bij jou past.</p>
		<button class="btn demo-button" id="belmijterugbutton">Bel mij terug</button>
		@endif
	</div>
</div>