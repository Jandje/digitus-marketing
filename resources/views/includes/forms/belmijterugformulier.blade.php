<div id="belmijterugformulier">
	<div class="belmijterugformulier">
		<div class="titel">
			<h2 class="pull-left">Bel mij terug</h2>
			<div id="close-form">
				<button type="button" class="close" style="font-size:22pt !important;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span> </button>
			</div>
		</div>
		<div class="clearfix"></div>
		<hr>
		{!! Form::open(array('url'=>'belmijterug')) !!}
		<div class="form-group">
			{!! Form::text('naam', null, array('class'=>'form-control', 'placeholder'=>'Naam..')) !!}
		</div>
		<div class="form-group">
			{!! Form::input('tel', 'telefoonnummer', null, array('class'=>'form-control', 'placeholder'=>'Telefoonnummer..')) !!}
		</div>
		<div class="clearfix"></div>
		<hr>
		<div class="form-group">
			<p class="pull-left"><span class="glyphicon glyphicon-lock"></span> Je gegevens zijn veilig</p>
			{!! Form::submit('Bel mij terug!', array('class'=>'btn demo-button pull-right')) !!}
		</div>
	</div>
</div>

<script>
	
</script>