<!-- BEGIN SIDEBAR -->
<!-- BEGIN MENU -->
<div class="page-sidebar" id="main-menu"> 
  <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
    @include('includes.admin.menus.user')
    <!-- BEGIN SIDEBAR MENU --> 
    <p class="menu-title">BROWSE<span class="pull-right"><a href="javascript:;"><i class="fa fa-refresh"></i></a></span></p>
    <ul>  
      <!-- BEGIN SELECTED LINK -->
      <li {{ Request::is('jandje') ? 'class=active' : '' }}>
        <a href="{!! action('Admin\AdminController@index') !!}">
          <i class="icon-custom-home"></i>
          <span class="title">Dashboard</span>
          <span class="selected"></span>
          {{-- <span class="badge badge-important pull-right">5</span> --}}
        </a>
      </li>
      <!-- END SELECTED LINK -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/blog') || Request::is('jandje/blog/*') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-pencil"></i>
          <span class="title">Blogposts</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/blog') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminBlogController@index') !!}">Alle Posts</a></li>
          <li {{ Request::is('jandje/blog/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminBlogController@create') !!}">Nieuwe Post</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/media') || Request::is('jandje/media/*') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-camera-retro"></i>
          <span class="title">Media</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/media') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminMediaController@index') !!}">Alle Afbeeldingen</a></li>
          <li {{ Request::is('jandje/media/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminMediaController@create') !!}">Nieuwe Afbeelding</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/diensten') || Request::is('jandje/diensten/*') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-file-text"></i>
          <span class="title">Diensten</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/diensten') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminDienstController@index') !!}">Alle Diensten</a></li>
          <li {{ Request::is('jandje/diensten/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminDienstController@create') !!}">Nieuwe Dienst</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/cases') || Request::is('jandje/cases/*') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-briefcase"></i>
          <span class="title">Cases</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/cases') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminCasesController@index') !!}">Alle Cases</a></li>
          <li {{ Request::is('jandje/cases/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminCasesController@create') !!}">Nieuwe Case</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/users') || Request::is('jandje/users/*') || Request::is('jandje/member/create') || Request::is('jandje/member/*/edit') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-users"></i>
          <span class="title">Gebruikers</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/users') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminUserController@index') !!}">Alle Gebruikers</a></li>
          <li {{ Request::is('jandje/users/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminUserController@create') !!}">Nieuwe Gebruiker</a></li>
          <li {{ Request::is('jandje/member/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminUserController@createMember') !!}">Nieuwe Member</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN SELECTED LINK -->
      <li {{ Request::is('jandje/cursisten') || Request::is('jandje/cursisten/*') ? 'class=active' : '' }}>
        <a href="{!! action('Admin\AdminCursistenController@index') !!}">
          <i class="fa fa-graduation-cap"></i>
          <span class="title">Cursisten</span>
          <span class="selected"></span>
          {{-- <span class="badge badge-important pull-right">5</span> --}}
        </a>
      </li>
      <!-- END SELECTED LINK -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/cursus') || Request::is('jandje/cursus/*') || Request::is('jandje/videos') || Request::is('jandje/videos/*') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-institution"></i>
          <span class="title">Cursussen</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/cursus') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminCursusController@index') !!}">Alle Cursussen</a></li>
          <li {{ Request::is('jandje/cursus/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminCursusController@create') !!}">Nieuwe Cursus</a></li>
          <li {{ Request::is('jandje/videos') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminVideosController@index') !!}">Alle Videos</a></li>
          <li {{ Request::is('jandje/videos/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminVideosController@create') !!}">Nieuwe Video</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN ONE LEVEL MENU -->
      <li {{ Request::is('jandje/recensies') || Request::is('jandje/recensies/*') ? 'class=active' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-comments"></i>
          <span class="title">Recensies</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {{ Request::is('jandje/recensies') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminRecensieController@index') !!}">Alle Recensies</a></li>
          <li {{ Request::is('jandje/recensies/create') ? 'class=active' : '' }}><a href="{!! action('Admin\AdminRecensieController@create') !!}">Nieuwe Recensie</a></li>
        </ul>
      </li>
      <!-- END ONE LEVEL MENU -->
      <!-- BEGIN BADGE LINK -->
      {{-- <li class="">
        <a href="#">
          <i class="fa fa-envelope"></i>
          <span class="title">Link 2</span>
          <span class="badge badge-disable pull-right">203</span>
        </a>
      </li> --}}
      <!-- END BADGE LINK -->     
      <!-- BEGIN SINGLE LINK -->
      {{-- <li class="">
        <a href="#">
          <i class="fa fa-flag"></i>
          <span class="title">Link 3</span>
        </a>
      </li> --}}
      <!-- END SINGLE LINK -->    
      <!-- BEGIN TWO LEVEL MENU -->
      <li {{ Request::is('jandje/rollen') || Request::is('jandje/rollen/*') || Request::is('jandje/categorieen') || Request::is('jandje/categorieen/*') || Request::is('jandje/icons') ? 'class=open' : '' }}>
        <a href="javascript:;">
          <i class="fa fa-folder-open"></i>
          <span class="title">Extras</span>
          <span class="arrow"></span>
        </a>
        <ul class="sub-menu" {{ Request::is('jandje/rollen') || Request::is('jandje/rollen/*') || Request::is('jandje/categorieen') || Request::is('jandje/categorieen/*') || Request::is('jandje/icons') ? 'style=display:block' : '' }}>
          <li {{ Request::is('jandje/icons') ? 'class=active' : '' }}>
            <a href="{!! action('Admin\AdminController@icons') !!}">
              <span class="title">Icons</span>
            </a>
          </li>
          <li {{ Request::is('jandje/categorieen') || Request::is('jandje/categorieen/*')  ? 'class=open' : '' }}>
            <a href="javascript:;"><span class="title">Categorieen</span><span class="arrow "></span></a>
            <ul class="sub-menu" {{ Request::is('jandje/categorieen') || Request::is('jandje/categorieen/*') ? 'style=display:block' : '' }}>
              <li {{ Request::is('jandje/categorieen')  ? 'class=active' : '' }}><a href="{!! action('Admin\AdminCategorieController@index') !!}">Alle Categorieen</a></li>
              <li {{ Request::is('jandje/categorieen/create')  ? 'class=active' : '' }}><a href="{!! action('Admin\AdminCategorieController@create') !!}">Nieuwe Categorie</a></li>
            </ul>
          </li>
          <li {{ Request::is('jandje/rollen') || Request::is('jandje/rollen/*')  ? 'class=active' : '' }}>
            <a href="javascript:;"><span class="title">Rollen</span><span class="arrow "></span></a>
            <ul class="sub-menu" {{ Request::is('jandje/rollen') || Request::is('jandje/rollen/*') ? 'style=display:block' : '' }}>
              <li {{ Request::is('jandje/rollen')  ? 'class=active' : '' }}><a href="{!! action('Admin\AdminRollenController@index') !!}">Alle Rollen</a></li>
              <li {{ Request::is('jandje/rollen/create')  ? 'class=active' : '' }}><a href="javascript:;">Nieuwe Rol</a></li>
            </ul>
          </li>
        </ul>
      </li>
      <!-- END TWO LEVEL MENU -->     
    </ul>
    <!-- END SIDEBAR MENU -->
  </div>
</div>