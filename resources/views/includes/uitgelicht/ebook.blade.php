<div id="uitgelicht">
	<div class="container">
		<div class="col-md-12 ebook-download-holder">
			<div class="col-md-3">
				<img src="http://www.digitusmarketing.nl/uploads/images/ebooks/ebook-plank.png" alt="" class="ebook-uitgelicht">
			</div>
			<div class="col-md-9 counter-blok">
				<div class="titel-blok">
					<h2>Vul onderstaand formulier in om het e-book GRATIS te downloaden.</h2>
				</div>
				<div class="ebook-counter">
					<div class="col-md-4">
						<div class="col-md-2">
							<span class="teller"></span>
						</div>
						<div class="col-md-2">
							<span class="teller"></span>
						</div>
						<div class="col-md-2">
							<span class="teller">1</span>
						</div>
						<div class="col-md-2">
							<span class="teller">2</span>
						</div>
						<div class="col-md-2">
							<span class="teller">4</span>
						</div>
						<div class="col-md-2">
							<span class="teller">8</span>
						</div>
					</div>
					<p class="ebook-downloads pull-left">Aantal downloads</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="ebook-download">
	<div class="container">
		<div class="col-md-12">
			<div class="ebook-download">
				<button class="btn demo-button" id="download-ebook">Download e-book</button>
			</div>
			<div class="ebook-download-formulier">
				{!! Form::open() !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>