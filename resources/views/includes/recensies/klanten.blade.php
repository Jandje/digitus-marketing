<h3>Wat klanten over ons zeggen:</h3>
<div id="recensies-slider">
	<!-- start Basic Jquery Slider -->
	<ul class="bjqs">
	@foreach($recensies as $recensie)
		<li>
			<p class="text-left recensie-tekst">{!! $recensie->content !!}</p>
			<div class="klant-info">
				<h3 class="klant">{!! $recensie->klantnaam !!}</h3><a href="{{ url($recensie->link) }}" rel="nofollow" target="_blank">{!! $recensie->bedrijf !!}</a>
			</div>
		</li>
	@endforeach
	</ul>
	<!-- end Basic jQuery Slider -->
</div>