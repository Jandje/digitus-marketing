@extends('layouts.detail')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
	<meta property="fb:admins" content="100000866281800" />
	<meta property="fb:admins" content="100000646526460" />
	<meta property="og:locale" content="nl_NL">
	<meta property="og:type" content="article">
	<meta property="og:image" content="{!! url($post->uitgelichte_afbeelding) !!}">
	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="og:title" content="{!! $post->titel !!}">
	<meta property="og:description" content="{!! $post->snippet !!}">
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
<div class="blog blog-post box">
	<div class="in-blog-post-image">
		<img src="{!! url($post->uitgelichte_afbeelding) !!}" alt="">
	</div>
	<div class="blog-post-content">
		<div class="pull-right dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" id="dropdownMenu1">
				meer <span class="caret"></span>
			</a>
			<ul class="dropdown-menu meeropties box" role="menu" aria-labelledby="dropdownMenu1">
				<li role="presentation"><a role="menuitem" tabindex="-1" href="http://www.facebook.com/sharer/sharer.php?u={!! action('BlogController@show',[$post->slug]) !!}&title={!! $post->titel !!}" target="_blank"><span class="fa fa-facebook"></span> Deel op Facebook</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="http://twitter.com/intent/tweet?text={!! $post->titel !!}&amp;url={!! action('BlogController@show',[$post->slug]) !!}" target="_blank"><span class="fa fa-twitter"></span> Deel op Twitter</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#disqus_thread"><span class="fa fa-pencil"></span> Reageer</a></li>
			</ul>
		</div>
		<div class="blog-titel in-post">
			<h3>{!! $post->titel !!}</h3>
		</div>
		<div class="blog-auteur-categorie">
			<ul>
				<li class="created">{{ $post->publishedAt() }}</li>
				<li class="auteur"><span class="icomoon icomoon-quill"></span>	<a href="" class="post-auteur">{{ $post->getAuthor() }}</a></li>
				<li class="categorie"><span class="glyphicon glyphicon-link"></span> <a href="{!! action('BlogController@show',[$post->slug]) !!}">{!! $post->categorie->naam !!}</a></li>
			</ul>					
		</div>
		<div class="clearfix"></div>
		<hr>
		<div class="blog-content">
			{!! $post->content !!}
		</div>
	</div>

</div>
<!-- Comments blok -->
<div id="disqus_thread"></div>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'digitusmarketingnl'; // required: replace example with your forum shortname
var disqus_identifier = '{!! $post->slug !!}';
var disqus_title = '{!! $post->titel !!}';
var disqus_url = '{!! action('BlogController@show',[$post->slug]) !!}';
var disqus_developer = 1;

/* * * DON'T EDIT BELOW THIS LINE * * */
(function() {
	var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
	(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>


<!-- /Comments blok -->

@stop

@section('related')
<div class="clearfix"></div>
<hr>
	<h3>Gerelateerde berichten</h3>
	@foreach($relatedposts as $relatedpost)
	@if($post->id != $relatedpost->id)
	<div class="col-md-4 blogpost">
		<div class="blog blog-post box">
			<div class="related-blog-post-image">
				<img src="{!! url($relatedpost->uitgelichte_afbeelding) !!}" alt="">
			</div>
			<div class="blog-post-content">
				<p class="blog-post-publish">{!! $relatedpost->publishedAt() !!}</p>
				<div class="blog-titel">
					<h5>{!! $relatedpost->titel !!}</h5>
				</div>
				<div class="blog-lezen">
					<a href="{{ action('BlogController@show',[$relatedpost->slug]) }}" class="btn nrml-button">Lees meer</a>
				</div>
			</div>
		</div>
	</div>
	@endif
	@endforeach
@stop

@section('sidebar')
	@include('includes.sidebar.groot')
@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	{!! Html::script('js/bjqs.js') !!}
	<script>
		var menuheight = $('div#menubars').height();

		$(document).ready(function(){
		  $('div.spacial').css('height',menuheight);
		});
	  $('.dropdown-toggle').dropdown();
	</script>
@stop