@extends('layouts.overview')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
		<div class="box introductie">
			<div class="titel">
				<h3>Kennis delen is kennis vermenigvuldigen!</h3>
			</div>
			<div class="content">
				<p>Het internet biedt ons de mogelijkheid om onze kennis en ervaring eenvoudig te delen en beschikbaar te stellen voor iedereen, waar en wanneer die wil.</p>
				<b class="bold">Houd onze blog in de gaten om op de hoogte te blijven van de laatste nieuwtjes, update en trends!</b>
			</div>
		</div>

@stop

@section('diensten')

	@foreach($blogposts as $post)
	<div class="col-md-6 blogpost">
		<div class="blog blog-post box">
			<div class="blog-post-image">
				<img src="{!! url($post->uitgelichte_afbeelding) !!}" alt="">
			</div>
			<div class="blog-post-content">
				<div class="pull-right dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" id="dropdownMenu1">
						meer <span class="caret"></span>
					</a>
					<ul class="dropdown-menu meeropties box" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="http://www.facebook.com/sharer/sharer.php?u={!! action('BlogController@show',[$post->slug]) !!}&title={!! $post->titel !!}" target="_blank"><span class="fa fa-facebook"></span> Deel op Facebook</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="http://twitter.com/intent/tweet?text={!! $post->titel !!}&amp;url={!! action('BlogController@show',[$post->slug]) !!}" target="_blank"><span class="fa fa-twitter"></span> Deel op Twitter</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="{!! action('BlogController@show',[$post->slug]) !!}#disqus_thread"><span class="fa fa-pencil"></span> Reageer</a></li>
					</ul>
				</div>
				<p class="blog-post-publish">{!! $post->publishedAt() !!}</p>
				<div class="blog-titel">
					<h3>{!! $post->titel !!}</h3>
				</div>
				<div class="blog-lezen">
					<a href="{{ action('BlogController@show',[$post->slug]) }}" class="btn nrml-button">Lees meer</a>
				</div>
				<div class="blog-auteur-categorie">
					<span class="icomoon icomoon-quill"></span>	<a href="" class="post-auteur">{{ $post->getAuthor() }}</a>
					<span class="glyphicon glyphicon-link"></span> {!! $post->categorie->naam !!}
				</div>
			</div>
		</div>
	</div>
	@endforeach
	<div class="clearfix"></div>
	{!! $blogposts->render() !!}
@stop
@section('sidebar')
	@include('includes.sidebar.klein')

@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	@include('includes.forms.belmijterugformulier')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	{!! Html::script('js/blog.js') !!}
@stop