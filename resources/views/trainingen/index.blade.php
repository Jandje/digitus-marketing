@extends('layouts.overview')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
		<div class="box introductie">
			<div class="titel">
				<h3 class="titel"><span class="glyphicon glyphicon-blackboard"></span> Trainingen</h3>
			</div>
		</div>
		<div class="box content">
			<h3>Hoi</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente unde ullam praesentium ab tenetur, sit error tempore consectetur odio. Autem culpa similique ea voluptate magnam. Possimus, molestiae enim. Voluptates, officiis!</p>
		</div>
@stop

@section('sidebar')
	@include('includes.sidebar.belmijterug')

	@include('includes.sidebar.groot')
@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
@stop