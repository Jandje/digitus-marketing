@extends('layouts.overview')

@section('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
@stop

@section('meta')
	@include('includes.meta.indexfollow')
@stop

@section('pixels')
@stop

@section('menu')
<div id="menubars">
	@include('includes.menus.hetmenu')
</div>
@stop

@section('content')
		<div class="box introductie">
			<div class="titel">
				<h3>Contact</h3>
			</div>
			<div class="content">
				<p>Benieuwd wat wij voor jou kunnen betekenen?<br/>
Neem telefonisch contact met ons op via 085 003 02 56 of stuur ons een mailtje.</p>
				<b class="bold" style="margin-top: 24px;">Algemene vragen kun je ook altijd stellen op onze <a href="https://www.facebook.com/DigitusMarketing" target="_blank">Facebook pagina</a></b>
			</div>
		</div>

@stop

@section('diensten')
	<div class="col-md-12 col-lg-12">
		<div class="box introductie">
			<div class="col-md-3 adres">
				<h3>Digitus Marketing</h3>
				<address>
					<p>Industriestraat 215a<br/>
					7553CP Hengelo(ov)</p>
					<p>Telefoon: 085 003 02 56<br/>
					info@digitusmarketing.nl</p>
					<p>Kvk: 52622746<br />
					BTW: NL.8505.25.469.B.01<br/>
					ABN Amro: 59.59.22.694</p>
				</address>
				{{-- <button class="btn nrml-button">Plan uw route</button> --}}
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-8 locatie">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2442.589700473182!2d6.777445899999991!3d52.250835800000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b80dfdc2b3e23f%3A0x4ee5f71cd7d956d2!2sIndustriestraat+215A%2C+Twentekanaal+Noord%2C+7553+CP+Hengelo!5e0!3m2!1snl!2snl!4v1429278463041" width="692" height="353" frameborder="0" style="border:0"></iframe>
			</div>
		</div>
	</div>
@stop
@section('sidebar')
	@include('includes.sidebar.belmijterug')

@stop

@section('footer')
	@include('includes.footer.mainfooter')
@stop

@section('footerscript')
	@include('includes.forms.belmijterugformulier')
	{!! Html::script('js/jquery-1.11.2.js') !!}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
@stop