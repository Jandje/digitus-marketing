@extends('layouts.backend')

@section('meta')
@include('includes.meta.noindexnofollow')
@stop

@section('pixels')
@stop

@section('headernav')
@include('includes.member.header.nav')
@stop

@section('menu')
@include('includes.member.menus.mainmenu')
@stop

@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="page-content"> 
	<div class="content">  
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">  
			<h3>Dashboard</h3>    
		</div>
		<!-- END PAGE TITLE -->
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		@if($member->role_id == '2' && $member->plan_id == '2')
		<div class="row">
			<div class="col-md-6">
				<div class="grid simple horizontal green">
					<div class="grid-title">
						<h4>Benodigdheden voor Facebook tab-app</h4>
					</div>
					<div class="grid-body">
						<a href="{!! action('Member\MemberBenodigdhedenController@index') !!}" class="btn btn-success">Bewerk / Bekijk</a>
					</div>
				</div>
			</div>
			@if($member->benodigd_id != null)
			<div class="col-md-6">
				<div class="grid simple horizontal red">
					<div class="grid-title">
						<h4>Trainingsvideos</h4>
					</div>
					<div class="grid-body">
						<a href="" class="btn btn-success">Bekijk trainingsvideos</a>
					</div>
				</div>
			</div>
			@endif
		</div>
		@endif
		@if($member->role_id == '2' && $member->plan == 'gratis-demo')
			HEY HOI
		@endif
		<!-- END PLACE PAGE CONTENT HERE -->

	</div>
</div>
<!-- END PAGE CONTAINER -->

@stop

@section('footer')
@include('includes.member.footer.mainfooter')
@stop

@section('footerscript')

@stop