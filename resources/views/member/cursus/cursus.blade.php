@extends('layouts.backend')

@section('meta')
@include('includes.meta.noindexnofollow')
@stop

@section('pixels')
@stop

@section('headernav')
@include('includes.member.header.nav')
@stop

@section('menu')
@include('includes.member.menus.mainmenu')
@stop

@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="page-content"> 
	<div class="content">  
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">  
			<h3>{!! $cursus->naam !!}</h3>    
		</div>
		<div class="grid simple vertical green">
			<div class="grid-body">
				{!! $cursus->content !!}
			</div>
		</div>
		<!-- END PAGE TITLE -->
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="row">
		@if($member->cursussen->contains(1) && $member->benodigd_id == null)
			<div class="col-md-6 col-md-offset-3">
				<div class="grid simple horizontal green">
					<div class="grid-title no-border">
						<h3>We hebben nog wat nodig!</h3>
					</div>
					<div class="grid-body no-border">
						<p>Voordat wij bezig kunnen met je Facebook tab-app, hebben wij nog enkele bestanden en gegevens van je sportschool nodig.</p>
						<p>Klik op onderstaande button om deze benodigdheden aan te leven en gelijk te beginnen met het bekijken van de video's!</p>
						<a href="{!! action('Member\MemberBenodigdhedenController@create') !!}" class="btn btn-success">Bestanden aanleveren</a>
					</div>
				</div>
			</div>
		@else
			@foreach($videos as $video)
				@if($video->plan_id == '2' && $member->plan_id == '2')
				<div class="col-md-6 video">
					<div class="grid simple horizontal red">
						<div class="grid-title">
							<h4>{!! $video->titel !!}</h4>
							<small>{!! $video->plan->naam !!}</small>
						</div>
						<div class="grid-body">
							{!! $video->link !!}
						</div>
					</div>
				</div>
				@elseif($video->plan_id == '3' && $member->plan_id == '3' || $member->plan_id == '2' && $video->plan_id == '3')
				<div class="col-md-6 video">
					<div class="grid simple horizontal purple">
						<div class="grid-title">
							<h4>{!! $video->titel !!}</h4>
							<small>{!! $video->plan->naam !!}</small>
						</div>
						<div class="grid-body">
							{!! $video->link !!}
						</div>
					</div>
				</div>
				@elseif($video->plan_id == '2' && $member->plan_id == '3')
				<div class="col-md-6 video video-disabled">
					<div class="grid simple horizontal green">
						<div class="grid-title">
							<h4>{!! $video->titel !!}</h4>
							<small>{!! $video->plan->naam !!}</small>
						</div>
						<div class="grid-body">
							<img src="{!! url($video->thumbnail) !!}" alt="" class="video-plaatje">
						</div>
					</div>
				</div>
				@else
				<div class="col-md-6 video">
					<div class="grid simple horizontal blue">
						<div class="grid-title">
							<h4>{!! $video->titel !!}</h4>
							<small>{!! $video->plan->naam !!}</small>
						</div>
						<div class="grid-body">
							{!! $video->link !!}
						</div>
					</div>
				</div>
				@endif
			@endforeach
		@endif
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->

	</div>
</div>
<!-- END PAGE CONTAINER -->

@stop

@section('footer')
@include('includes.member.footer.mainfooter')
@stop

@section('footerscript')

@stop