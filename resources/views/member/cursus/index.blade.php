@extends('layouts.backend')

@section('meta')
@include('includes.meta.noindexnofollow')
@stop

@section('pixels')
@stop

@section('headernav')
@include('includes.member.header.nav')
@stop

@section('menu')
@include('includes.member.menus.mainmenu')
@stop

@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="page-content"> 
	<div class="content">  
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">  
			<h3>Cursussen</h3>    
		</div>
		<!-- END PAGE TITLE -->
		<!-- BEGIN PlACE PAGE CONTENT HERE -->
		<div class="row">
		@foreach($cursussen as $cursus)
			@if($member->heeftCursus($cursus->id))

			<div class="col-md-6">
				<div class="grid simple horizontal green">
					<div class="grid-title">
						<h4>{!! $cursus->naam !!}</h4>
					</div>
					<div class="grid-image">
						<img src="{!! url($cursus->afbeelding) !!}" alt="{!! $cursus->naam !!}">
					</div>
					<div class="grid-body">
						<a href="{!! action('Member\MemberCursussenController@show',[$cursus->slug]) !!}" class="btn btn-success">Ga naar cursus</a>
					</div>
				</div>
			</div>
			@else
			<div class="col-md-6 disabled">
				<div class="grid simple horizontal green">
					<div class="grid-title">
						<h4>{!! $cursus->naam !!}</h4>
					</div>
					<img src="{!! url($cursus->afbeelding) !!}" alt="{!! $cursus->naam !!}">
					<div class="grid-body">
						<a href="#" class="btn btn-success disabled">Ga naar cursus</a>
						<a href="#" class="btn btn-primary">Koop cursus</a>
					</div>
				</div>
			</div>
			@endif
		@endforeach
		</div>
		<!-- END PLACE PAGE CONTENT HERE -->

	</div>
</div>
<!-- END PAGE CONTAINER -->

@stop

@section('footer')
@include('includes.member.footer.mainfooter')
@stop

@section('footerscript')

@stop