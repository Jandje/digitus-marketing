jQuery(function($) {

	$(document).ready(function(){


		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};

		if( !isMobile.any() ) {

			var floaterScrollTop = 0;
			var floatMenuOriginalTopPos = $('.spacial').offset().top + 50;
			$(window).scroll(function() {
				floaterScrollTop = $(window).scrollTop();
				if(floaterScrollTop >= floatMenuOriginalTopPos && $('html').hasClass('scroll') == false) {
					$('html').addClass('scroll');
					$('#loginmenubar').slideUp();
				}
				else if(floaterScrollTop < floatMenuOriginalTopPos && $('html').hasClass('scroll') ){
					$('html').removeClass('scroll');
					$('#loginmenubar').slideDown();
				}
			});
		}

		var menuheight = $('div#menubars').height();

		$('div.spacial').css('height',menuheight);
		$('.dropdown-toggle').dropdown();

		$('a.sidemenu-toggle').on('click', function()
		{
			$('html').toggleClass('sidemenu-open');
		});
		$('div#close-sidemenu').on('click', function()
		{
			$('html').removeClass('sidemenu-open');
		});


		$('#belmijterugbutton').on('click', function(){
			$('#belmijterugformulier').fadeIn();
			$('#belmijterugformulier').addClass("-on");
		    $(window).resize(function(){
		        $('.belmijterugformulier').css({'margin-top':-Number($('.belmijterugformulier').height() /2 )});
		    });

		    setTimeout(function(){
		    	allGood = true;
		    	buitenKlik();
		    },1000);
		});

		$('div#close-form').on('click', function(){
			$('#belmijterugformulier').fadeOut("-on");
			$('#belmijterugformulier').removeClass("-on");
			allGood = false;

		});

		var allGood = true;

		function buitenKlik(e){

			$(document).on('click', function(e) {
				var $el = $('.belmijterugformulier');

				if(allGood == true) {
				
					if($(e.target).is('.belmijterugformulier') || $(e.target).is('.belmijterugformulier > *') || $(e.target).is('.belmijterugformulier > * > *') || $(e.target).is('.belmijterugformulier > * > * > *') || $(e.target).is('input.btn.btn-success') || $(e.target).is('input[type="checkbox"]')){
						
					} else {
						$('#belmijterugformulier').fadeOut();
						$('#belmijterugformulier').removeClass("-on");
						allGood = false;
					}
				}
			});
		}

		// var isMobile = {
		//     Android: function() {
		//         return navigator.userAgent.match(/Android/i);
		//     },
		//     BlackBerry: function() {
		//         return navigator.userAgent.match(/BlackBerry/i);
		//     },
		//     iOS: function() {
		//         return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		//     },
		//     Opera: function() {
		//         return navigator.userAgent.match(/Opera Mini/i);
		//     },
		//     Windows: function() {
		//         return navigator.userAgent.match(/IEMobile/i);
		//     },
		//     any: function() {
		//         return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		//     }
		// };

		// if( !isMobile.any() ) {

		//     var currentScrollTop = 0;
		//     var barMenuOriginalTopPos = $('.blogroll').offset().top + 175;
		//     $(window).scroll(function() {
		//         currentScrollTop = $(window).scrollTop();
		//         if(currentScrollTop >= barMenuOriginalTopPos && $('#sidebar-groot').hasClass('fixElementToTop') == false) {
		//             $('#sidebar-groot').addClass('fixElementToTop');
		//             $('.fixElementToTop').css('top', -($('.fb-like-box.fb_iframe_widget').height() + $('div.blogroll').height() * 0.525 ));
		//         }
		//         else if(currentScrollTop < barMenuOriginalTopPos && $('#sidebar-groot').hasClass('fixElementToTop') ){
		//             $('#sidebar-groot').removeClass('fixElementToTop');
		//         }
		//     });

		// }
		

	});
});