jQuery(function($) {

	$(document).ready(function(){

		var floaterScrollTop = 0;
		var floatMenuOriginalTopPos = $('.spacial').offset().top + 390;
		$(window).scroll(function() {
			floaterScrollTop = $(window).scrollTop();
			if(floaterScrollTop >= floatMenuOriginalTopPos && $('html').hasClass('scroll') == false) {
				$('html').addClass('scroll');
				$('#loginmenubar').slideUp();
			}
			else if(floaterScrollTop < floatMenuOriginalTopPos && $('html').hasClass('scroll') ){
				$('html').removeClass('scroll');
				$('#loginmenubar').slideDown();
			}
		});

		$('div#close-sidemenu').on('click', function()
		{
			$('html').removeClass('sidemenu-open');
		});

		var menuheight = $('div#menubars').height();

		$('div.spacial').css('height',menuheight);
		$('.dropdown-toggle').dropdown();

		$('#recensies-slider').bjqs();

		$('ol.bjqs-markers.h-centered').css('left','0 !important');
		$("#banner-fade").css({"display":"block","margin":"0 auto"});
		$("#slider-banner").css({"display":"block","margin":"0 auto"});
		$('#banner-fade li.bjqs-slide').css('width',$('#banner-fade .bjqs-wrapper').width());

		$('.sidemenu-toggle').on('click', function() {
			$('html').toggleClass('sidemenu-open');
		});

	});
});